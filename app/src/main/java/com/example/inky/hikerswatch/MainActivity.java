package com.example.inky.hikerswatch;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

import java.util.List;
import java.util.Locale;

public class MainActivity extends AppCompatActivity
{

    LocationManager locationManager;
    LocationListener locationListener;

    TextView txtLatitude, txtLongitude, txtAccuracy, txtAltitude, txtAddress;

    String address;

    //Side note on imageview:
    //Add images to xxhdhdpi to drawables
    //Imageview-> Attributes -> fitXY


    @SuppressLint("MissingPermission")
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults)
    {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == 1) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED)
            {
                startListening();
            }
        }

    }

    @SuppressLint("MissingPermission")
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        txtLatitude = (TextView) findViewById(R.id.txt_Latitiude);
        txtLongitude = (TextView) findViewById(R.id.txt_Longitude);
        txtAccuracy = (TextView) findViewById(R.id.txt_Accuracy);
        txtAltitude = (TextView) findViewById(R.id.txt_Altitude);
        txtAddress = (TextView) findViewById(R.id.txt_Address);

        address ="";



        locationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);
        locationListener = new LocationListener()
        {
            @Override
            public void onLocationChanged(Location location)
            {
                updateLocationInfo(location);

            }

            @Override
            public void onStatusChanged(String provider, int status, Bundle extras)
            {

            }

            @Override
            public void onProviderEnabled(String provider)
            {

            }

            @Override
            public void onProviderDisabled(String provider)
            {

            }
        };

        if (Build.VERSION.SDK_INT < 23)
        {
            startListening();
        }
        else
        {
            if(ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED)
            {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);

            }
            else
            {
                locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER,0,0, locationListener);

                Location loc = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
                if(loc != null)
                {
                    updateLocationInfo(loc);
                }


            }
        }

    }

    public void updateLocationInfo(Location location)
    {

        //Log.i("Location", location.toString());
        txtLatitude.setText("Latitude: " + String.valueOf(location.getLatitude()));
        txtLongitude.setText("Longitude: " + String.valueOf(location.getLongitude()));
        txtAccuracy.setText("Accuracy: " + String.valueOf(location.getAccuracy()));
        txtAltitude.setText("Altitude: " + String.valueOf(location.getAltitude()));

        Geocoder geocoder = new Geocoder(getApplicationContext(), Locale.getDefault());

        try
        {
            List<Address> addressList = geocoder.getFromLocation(location.getLatitude(), location.getLongitude(),1);
            address = "No address found.";

            if(addressList != null && addressList.size() > 0)
            {
                address = "Address: \n";

                if(addressList.get(0).getSubThoroughfare() != null)
                {
                    address += addressList.get(0).getSubThoroughfare() + "\n";
                }
                if(addressList.get(0).getThoroughfare() != null)
                {
                    address += addressList.get(0).getThoroughfare() + "\n";
                }
                if(addressList.get(0).getLocality() != null)
                {
                    address += addressList.get(0).getLocality() + "\n";
                }
                if(addressList.get(0).getAdminArea() != null)
                {
                    address += addressList.get(0).getAdminArea() + "\n";
                }

                if(addressList.get(0).getPostalCode() != null)
                {
                    address += addressList.get(0).getPostalCode() + "\n";
                }
                if(addressList.get(0).getCountryName() != null)
                {
                    address += addressList.get(0).getCountryName() + "\n";
                }


            }
            txtAddress.setText(address);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    public void startListening()
    {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED)
        {
            return;
        }
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, locationListener);
    }
}
